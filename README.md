![Build Status](https://gitlab.com/pages/jekyll/badges/master/build.svg)
![Jekyll Version](https://img.shields.io/gem/v/jekyll.svg)

---

Motivado pelo presente surto do vírus COVID-19, o presente repositório contém um site escrito em [Jekyll](https://jekyllrb.com/docs/) para providenciar uma lista de recursos a utilizar.

Uma agradecimento especial à ANSOL pelo seu envolvimento e pelos inúmeros recursos adicionados pelas mãos deles.


## Contribuir

Sabes de mais software que devia constar na lista? Então faz o teu fork, edita o ficheiro `index.md` e faz merge request com as tuas alterações para este repositório. Isso permitir-nos-á correr a nossa curta e ligeira pipeline CI/CD (garante que as alterações não corromporam o site e faz deploy automaticamente após o merge).
